python-pynndescent (0.5.11-1) unstable; urgency=medium

  [ Andreas Tille ]
  * Team upload.
  * New upstream version (Closes: #1057598)
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)

  [ Benjamin Drung ]
  * test: load pynndescent_bug_np.npz from relative path
  * Hard-code __version__ during build to fix running test during build

 -- Benjamin Drung <bdrung@debian.org>  Sat, 01 Jun 2024 00:37:17 +0200

python-pynndescent (0.5.8-2) unstable; urgency=medium

  * Team Upload.
  * Skip some tests on arm, which seems to be rather due
    to issues with other packages
  * d/t/run-unit-test: test supported pyvers instead of release (-r)
  * Disable i386 in salsa-ci.yml
  * Test dep on python3-all

 -- Nilesh Patra <nilesh@debian.org>  Sun, 12 Feb 2023 01:31:13 +0530

python-pynndescent (0.5.8-1) unstable; urgency=medium

  [ Andreas Tille ]
  * Team Upload.
  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * Simplify salsa-ci.yml

  [ Nilesh Patra ]
  * Add upstream patches to fix FTBFS

 -- Nilesh Patra <nilesh@debian.org>  Sat, 11 Feb 2023 23:29:37 +0530

python-pynndescent (0.5.7-1) unstable; urgency=medium

  * New upstream version
  * Move package to Debian Python Team
  * Switch test framework from nose to pytest
  * Standards-Version: 4.6.1 (routine-update)
  * Switch autopkgtest to pytest
  * Fix Homepage

 -- Andreas Tille <tille@debian.org>  Tue, 19 Jul 2022 08:21:53 +0200

python-pynndescent (0.5.2+dfsg-1) unstable; urgency=medium

  * Team Upload.
  [ Andreas Tille ]
  * New upstream version
  * Exclude *.pyc files from upstream source
  * Make package Architecture dependent to exclude 32bit architectures
    where the tests are failing
  * Remove empty boilerplate

  [ Nilesh Patra ]
  * d/rules: Enable build time tests respecting DEB_BUILD_OPTIONS
  * d/tests/control: Restrict testing architectures to supported
    build architectures
  * d/salsa-ci.yml: Do run build on i386

 -- Nilesh Patra <nilesh@debian.org>  Sat, 03 Jul 2021 23:45:46 +0530

python-pynndescent (0.5.1-2) unstable; urgency=medium

  * Section: Python
  * Spelling fix for long description
  * Remove trailing whitespace in debian/copyright (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Fri, 05 Feb 2021 14:31:16 +0100

python-pynndescent (0.5.1-1) unstable; urgency=medium

  * Initial release (Closes: #980537)

 -- Andreas Tille <tille@debian.org>  Wed, 20 Jan 2021 16:17:17 +0100
